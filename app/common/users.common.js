/**
* This is the users common class.
* It contains all the methods with promises of users
* @class usersCommon
*/

module.exports = {
    /**
    * Action for delete the record in database
    *
    * @method update
    * @param {string} modelName - name of the model
    * @param {object} condition - condition
    * @return {promise} res - If query is sucess then it resolves deleted data otherwise rejects some error.
    */
    deleteHook: function (modelName, condition) {
        return new Promise(function (resolve, reject) {
            if (typeof modelName.getRelatedSchema == 'function') {
                modelName.getRelatedSchema(function (relatedCollection) {
                    modelName.findOne(condition).select().exec(function (err, doc) {
                        if (err) {
                            reject(err);
                        } else if (typeof doc != 'undefined' && doc !== null && doc != '') {
                            doc.remove(function (err, result) {
                                if (err) {
                                    reject(err);
                                } else {
                                    if (_.isEmpty(relatedCollection)) {
                                        reject(err);
                                    } else {
                                        _.forEach(relatedCollection, function (value) {
                                            var innerCondition = {};
                                            innerCondition[value.fieldName] = result._id;
                                            value.modelName.remove(innerCondition).exec();
                                        });
                                        resolve(doc);
                                    }
                                }
                            });
                        } else {
                            var error = {
                                httpstatus: 400,
                                msg: appMessage.common.error.badRequest.msg
                            }
                            reject(error);
                        }
                    });
                });
            }
        });
    }
}