module.exports = {
    common: {
        error: {
            noUserAvailable: {
                msg: "No User Available"
            },
            noToken: {
                msg: 'No token provided'
            },
            noPostAvailable: {
                msg: "No Post Available"
            },
            selfFriend: {
                msg: "user is not able to friend himself."
            },
            alreadyFriend: {
                msg: "You are already friend with the user."
            },
            noFriend: {
                msg: "there is no friend to remove."
            },
            alreadyLike: {
                msg: "You have already like the post."
            },
            badRequest: {
                msg: "please check something went wrong/ No data found"
            },
            cannotEdit: {
                msg: "Not able to edit other user details"
            }
        },
        success: {
            removedFriend: {
                msg: "friend removed successfully"
            },
            postUnlike: {
                msg: "Unliked successfully"
            },
            deleteComment: {
                msg: "comment deleted successfully"
            },
            deletePost: {
                msg: "post deleted successfully"
            },
            commentEdit: {
                msg: "comment edited successfully"
            }
        }
    }
}