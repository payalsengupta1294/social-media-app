/**
* This is the postComment model.
* Creating schema for postComment
*
* @class postCommentModel
*/
var postCommentSchema = new Schema({
    postId: { type: Schema.Types.ObjectId,ref: 'post', required: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users', required: true },
    comment: {type: String, required: true},
}, {
    timestamps: true,
    versionKey: false
});


/*
 * create model from schema
 */
var collectionName = 'postComment';
var postComment = mongoose.model('postComment', postCommentSchema, collectionName);


/*
 * export postComment model
 */
module.exports = postComment;