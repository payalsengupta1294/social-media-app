/**
* This is the add friends model.
* Creating schema for addFriends
*
* @class addFriendsModel
*/

var addFriendsSchema = new Schema({
    userId: { type: Schema.Types.ObjectId, ref: 'users' },
    friendUserId: { type: Schema.Types.ObjectId, ref: 'users' },
}, {
    timestamps: true,
    versionKey: false
});

/*
 * create model from schema
*/
addFriendsSchema.index({ userId: 1, friendUserId: 1 }, { unique: true });
var collectionName = 'addFriends';
var addFriends = mongoose.model('addFriends', addFriendsSchema, collectionName);


/*
 * export addFriends model
*/
module.exports = addFriends;