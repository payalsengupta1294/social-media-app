    /**
    * This is the postLike model.
    * Creating schema for postLike
    *
    * @class postLikeModel
    */
   var postLikeSchema = new Schema({
    postId: { type: Schema.Types.ObjectId,ref: 'post', required: true },
    createdBy: { type: Schema.Types.ObjectId, ref: 'users', required: true },
}, {
    timestamps: true,
    versionKey: false
});


/*
 * create model from schema
 */
postLikeSchema.index({ postId: 1, createdBy: 1 }, { unique: true });
var collectionName = 'postLike';
var postLike = mongoose.model('postLike', postLikeSchema,collectionName);


/*
 * export postLike model
 */
module.exports = postLike;