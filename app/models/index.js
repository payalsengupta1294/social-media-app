/*
 * Export all model files after require
*/
module.exports = {
  users: require('./users.js'),
  post: require('./post.js'),
  postLike: require('./postLike.js'),
  postComment: require('./postComment.js'),
  addFriends: require('./addFriends.js'),
};