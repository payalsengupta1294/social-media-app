    /**
    * This is the user model.
    * Creating schema for user
    *
    * @class userModel
    */
   var userSchema = new Schema({
    name: {type: String},
    email: {type: String,lowercase: true},
    mobileNo: {type: String},
    password: {type: String},
    dob: {type: Date},
    googleId: {type: String},
    image:{type: String},
    isActive: {type: Boolean, default: true},
    isDelete: {type: Boolean, default: false},
}, {
    timestamps: true,
    versionKey: false
});


/*
 * create model from schema
 */
var collectionName = 'users';
var users = mongoose.model('users', userSchema,collectionName);


/*
 * export users model
 */
module.exports = users;