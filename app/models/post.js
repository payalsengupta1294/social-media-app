/**
* This is the post model.
* Creating schema for post
*
* @class postModel
*/
var postSchema = new Schema({
    description: { type: String, required: true },
    createdBy: {
        type: Schema.Types.ObjectId,
        ref: 'users'
    },
    isActive: { type: Boolean, default: true },
    isDelete: { type: Boolean, default: false },
}, {
    timestamps: true,
    versionKey: false
});

/* getting related schema*/
postSchema.statics.getRelatedSchema = function (cb) {
    cb([
        { modelName: models.postLike, fieldName: "postId" },
        { modelName: models.postComment, fieldName: "postId" }
    ]);
};

/*
 * create model from schema
 */
var collectionName = 'post';
var post = mongoose.model('post', postSchema, collectionName);


/*
 * export post model
 */
module.exports = post;