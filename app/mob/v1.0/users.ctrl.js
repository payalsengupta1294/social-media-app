/**
 * This is the user class.
 * It contains all the methods user
 * @class userController
 */
var bcrypt = require("bcrypt");
var jwt = require('jsonwebtoken');
var multiparty = require('multiparty');
var salt = bcrypt.genSaltSync(10);
module.exports = {

    /**
     * Action for adding user by admin
     *
     * @method addAdminUsers
     * @param {req} request
     * @param {res} response
     * @return {status} res -If it returns error then the status is false otherwise true. 
     */
    addUser: function (req, res) {
        var modelName = models.users;
        var form = new multiparty.Form();
        form.parse(req, function (err, fields, files) {
            var postData = fields;
            if (files.image && typeof files.image != 'undefined') {
                // need to change image path from environment file as per the server hosting
                var newPath = process.env.IMAGE_PATH + files.image[0].originalFilename;
                console.log(newPath)
                fs.rename(files.image[0].path, newPath, function (err) {
                    if (err) {
                        var error = {
                            httpstatus: 400,
                            msg: 'Unable to save'
                        };
                        res.send(error)
                    } else {
                        // getting bufferdata of the image already stored in public folder
                        fs.readFile(newPath, function (err, bufferdata) {
                            // Converting bufferdata to base64 //
                            base64 = bufferdata.toString('base64')
                            var obj = {
                                name: postData.name[0],
                                email: postData.email[0],
                                mobileNo: postData.email[0],
                                password: bcrypt.hashSync(postData.password[0], salt),
                                dob: postData.dob[0],
                                image: base64
                            }
                            // to save users in database
                            var users = new modelName(obj);
                            users.save(function (err, data) {
                                if (err) return res.json(err, 400);
                                res.status(200).json(data)
                            });
                        })
                    }
                })
            } else {
                var objWithoutImage = {
                    name: postData.name[0],
                    email: postData.email[0],
                    mobileNo: postData.email[0],
                    password: bcrypt.hashSync(postData.password[0], salt),
                    dob: postData.dob[0],
                }
                var users = new modelName(objWithoutImage);
                users.save(function (err, data) {
                    if (err) return res.json(err, 400);
                    res.status(200).json(data)
                });
            }
        });
    },

    /**
    * Action for user login
    * 
    * @method login
    * @param {req} request
    * @param {res} response
    * @return {status} res -If it returns error then the status is false otherwise true. 
    */

    login: function (req, res) {
        var modelName = models.users;
        var condition = {
            email: req.body.email
        }
        var fields = [""];
        modelName.findOne(condition).lean().select(fields).exec(function (err, data) {
            if (typeof data != 'undefined' && data != '' && data != null) {
                var result = bcrypt.compareSync(req.body.password, data.password);
                if (result == true) {
                    /*
                     * JWT token generation and set token to header
                     */
                    var token = jwt.sign(data, 'test');
                    res.setHeader('x-access-token', token);
                    var data = {
                        httpstatus: 200,
                        msg: "Logged In Successfully"
                    }
                    res.send(data)
                } else {
                    err = {
                        httpstatus: "401",
                        msg: "incorrect password."
                    };
                    res.send(err)
                }
            } else {
                err = {
                    httpstatus: "401",
                    msg: "user not found."
                };
                res.send(err)
            }
        })
    },

    /**
    * Action for getting user Details
    *
    * @method getUserDetails
    * @param {req} request
    * @param {res} response
    * @return {data} res - If data is there then it returns user Details otherwise it returns message.
    */
    getUserDetails: function (req, res) {
        var userId = requestUserId
        models.users.findOne({
            _id: userId
        }).exec(function (err, data) {
            if (err) {
                return res.json(err, 400);
            } else {
                if (_.isEmpty(data)) {
                    var err = {
                        httpstatus: 200,
                        msg: appMessage.common.error.noUserAvailable.msg
                    }
                    res.send(err)
                } else {
                    res.status(200).json(data)
                }
            }
        });
    },

    /**
     * Action for updating user details
     *
     * @method editUser
     * @param {req} request
     * @param {res} response
     * @return {status} res -If it returns error then the status is false otherwise it will update the user details with success response. 
     */
    editUser: function (req, res) {
        var modelName = models.users;
        var condition = {
            '_id': req.params.id,
        };
        var updateParams = req.body;
        if (requestUserId == req.params.id) {
            modelName.findOneAndUpdate(condition, updateParams, { new: true }).then(function (err, data) {
                if (err) return res.json(err, 400);
                res.status(200).json(data)
            });
        } else {
            var error = {
                httpstatus: 400,
                msg: appMessage.common.error.cannotEdit.msg
            }
            res.send(error)
        }
    },

    /**
    * Action for adding friends
    *
    * @method addFriends
    * @param {req} request
    * @param {res} response
    * @return {data} res - If it returns error then the status is false otherwise true and it will add data.
    */
    addFriends: function (req, res) {
        var modelName = models.addFriends;
        var postData = req.body;
        postData.userId = requestUserId;
        if (postData.userId != postData.friendUserId) {
            /* add friends */
            var posts = new modelName(postData);
            if (req.body.type == 'true') {
                posts.save(function (err, data) {
                    if (err) {
                        var error = {
                            httpstatus: 400,
                            msg: appMessage.common.error.alreadyFriend.msg
                        }
                        res.send(error)
                    } else {
                        res.status(200).json(data)
                    }
                });
            } else {
                /* delete friend */
                var condition = {
                    userId: requestUserId,
                    friendUserId: req.body.friendUserId
                };
                var fields = [''];
                modelName.findOne(condition).select(fields).exec(function (err, data) {
                    if (err) {
                        res.send(error)
                    } else {
                        if (typeof data !== 'undefined' && data !== '' && data !== null) {
                            data.remove(function (err) {
                                if (err) {
                                    res.status(400).json(err)
                                }
                                var data = {
                                    httpstatus: 200,
                                    msg: appMessage.common.success.removedFriend.msg
                                }
                                res.send(data)
                            })
                        } else {
                            var error = {
                                httpstatus: 400,
                                msg: appMessage.common.error.noFriend.msg
                            }
                            res.send(error)
                        }
                    }
                })
            }
        } else {
            var error = {
                httpstatus: 400,
                msg: appMessage.common.error.selfFriend.msg
            }
            res.send(error)
        }
    },
}