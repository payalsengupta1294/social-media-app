/**
 * This is the post class.
 * It contains all the methods post
 * @class postController
 */

module.exports = {

    /**
     * Action for add posts by the users
     *
     * @method addPost
     * @param {req} request
     * @param {res} response
     * @return {status} res -If postData is there then it returns posts otherwise it returns false. 
     */
    addPost: function (req, res) {
        var modelName = models.post;
        var postData = req.body;
        //createdby will be taken from x-access-token
        postData.createdBy = requestUserId;
        // to save post
        var posts = new modelName(postData);
        posts.save(function (err, data) {
            if (err) return res.json(err, 400);
            res.status(200).json(data)
        });
    },

    /**
    * Action for getting my all post and friends post
    *
    * @method getAllPost
    * @param {req} request
    * @param {res} response
    * @return {data} res - If data is there then it returns my post and friend post otherwise it returns message.
    */
    getAllPost: function (req, res) {
        var userId = requestUserId;
        var match = {};
        var insideMatch = {
            $or: [{
                "friend.userId": new mongoose.Types.ObjectId(userId)
            }, {
                createdBy: new mongoose.Types.ObjectId(userId)
            }]
        }
        if (typeof postId != 'undefined') {
            match._id = postId;
        }
        var agg = [{
            $match: match
        }, {
            $lookup: {
                from: "addFriends",
                localField: "createdBy",
                foreignField: "friendUserId",
                as: "friend",

            }
        }, {
            $unwind: {
                path: "$friend",
                preserveNullAndEmptyArrays: true
            }
        }, {
            $match: insideMatch
        }, {
            $group: {
                _id: "$_id",
                updatedAt: {
                    $first: "$updatedAt"
                },
                createdAt: {
                    $first: "$createdAt"
                },
                description: {
                    $first: "$description"
                },
                createdBy: {
                    $first: "$createdBy"
                },
                isDelete: {
                    $first: "$isDelete"
                },
                isActive: {
                    $first: "$isActive"
                },
            }
        }, {
            $sort: {
                createdAt: -1
            }
        }, {
            $lookup: {
                from: "users",
                localField: "createdBy",
                foreignField: "_id",
                as: "users"
            }
        }, {
            $lookup: {
                from: "postLike",
                localField: "_id",
                foreignField: "postId",
                as: "likes"
            }
        }, {
            $lookup: {
                from: "postComment",
                localField: "_id",
                foreignField: "postId",
                as: "comments"
            }
        }, {
            $addFields: {
                totalLikes: {
                    $size: "$likes"
                },
                totalComments: {
                    $size: "$comments"
                },
            }
        }, {
            $project: {
                "_id": 1,
                "id": "$_id",
                "description": 1,
                "createdAt": 1,
                "createdBy": 1,
                "totalLikes": 1,
                "totalComments": 1,
                "users._id": 1,
                "users.name": 1,
            }
        }];
        models.post.aggregate(agg, function (err, data) {
            if (err) {
                return res.json(err, 400);
            } else {
                if (_.isEmpty(data)) {
                    var err = {
                        httpstatus: 200,
                        msg: appMessage.common.error.noPostAvailable.msg
                    }
                    res.send(err)
                } else {
                    res.status(200).json(data)
                }
            }
        });
    },

    /**
    * Action for getting post Details by postId
    *
    * @method getPostDetails
    * @param {req} request
    * @param {res} response
    * @return {data} res - If data is there then it returns post Details otherwise it returns message.
    */

    getPostDetails: function (req, res) {
        var postId = req.params.id;
        var agg = [
            {
                $match: {
                    _id: new mongoose.Types.ObjectId(postId),
                }
            },
            {
                $lookup: {
                    from: "postLike",
                    localField: "_id",
                    foreignField: "postId",
                    as: "postLike"
                }
            }, {
                $lookup: {
                    from: "postComment",
                    localField: "_id",
                    foreignField: "postId",
                    as: "postComment"
                }
            },
        ];
        models.post.aggregate(agg, function (err, data) {
            if (err) {
                return res.json(err, 400);
            } else {
                if (_.isEmpty(data)) {
                    var err = {
                        httpstatus: 200,
                        msg: appMessage.common.error.noPostAvailable.msg
                    }
                    res.send(err)
                } else {
                    res.status(200).json(data)
                }
            }
        });
    },

    /**
   * Action for delete post along with post like and comments.
   *
   * @method postComment
   * @param {req} redeleteCommentquest
   * @param {res} response
   * @return {status} res -If res is success it will delete the post otherwise it returns false. 
   */
    deletePost: function (req, res) {
        var modelName = models.post;
        var postId = req.params.id;
        modelName.findOne({ _id: postId }).exec(function (err, data) {
            if (typeof data !== 'undefined' && data !== '' && data !== null) {
                // delete the post
                common.users.deleteHook(modelName, {
                    _id: postId
                }).then(function () {
                    var data = {
                        httpstatus: 200,
                        msg: appMessage.common.success.deletePost.msg
                    }
                    res.send(data)
                });
            } else {
                var error = {
                    httpstatus: 400,
                    msg: appMessage.common.error.badRequest.msg
                };
                res.send(error)
            }
        });

    },

    /**
     * Action for updating post
     *
     * @method editPost
     * @param {req} request
     * @param {res} response
     * @return {status} res -If it returns error then the status is false otherwise it will update the post with success response. 
     */
    editPost: function (req, res) {
        var modelName = models.post;
        var condition = {
            '_id': req.params.id,
        };
        var updateParams = req.body;
        modelName.findOneAndUpdate(condition, updateParams, { new: true }).then(function (err, data) {
            if (err) return res.json(err, 400);
            res.status(200).json(data)
        });
    },

    /**
    * Action for add likes to the post by users and remove like from the post which is already liked by the user.
    *
    * @method postLike
    * @param {req} request
    * @param {res} response
    * @return {status} res -If liketype is true is will add data or else it will remove likes from the post. 
    */
    postLike: function (req, res) {
        var modelName = models.postLike;
        var postData = req.body;
        //createdby will be taken from x-access-token
        postData.createdBy = requestUserId;
        // to save post likes
        var postLike = new modelName(postData);
        if (postData.likeType === "true") {
            postLike.save(function (err, data) {
                if (err) {
                    var err = {
                        httpstatus: 400,
                        msg: appMessage.common.error.alreadyLike.msg
                    }
                    res.send(err)
                } else {
                    res.status(200).json(data)
                }
            });
            /* remove like from post */
        } else {
            var condition = {
                createdBy: requestUserId,
                postId: req.body.postId
            };
            var fields = [''];
            modelName.findOne(condition).select(fields).exec(function (err, data) {
                if (err) {
                    res.send(err);
                } else {
                    if (typeof data !== 'undefined' && data !== '' && data !== null) {
                        // remove like from post if post is already liked
                        data.remove(function (err, data) {
                            if (err) {
                                res.send(err);
                            }
                            var data = {
                                httpstatus: 200,
                                msg: appMessage.common.success.postUnlike.msg
                            }
                            res.send(data)
                        })
                    } else {
                        var error = {
                            httpstatus: 400,
                            msg: appMessage.common.error.badRequest.msg
                        };
                        res.send(error)
                    }
                }
            });
        }
    },

    /**
    * Action for add comment to the post by users.
    *
    * @method postComment
    * @param {req} request
    * @param {res} response
    * @return {status} res -If postData is there then it returns likes otherwise it returns false. 
    */
    postComment: function (req, res) {
        var modelName = models.postComment;
        var postData = req.body;
        //createdby will be taken from x-access-token
        postData.createdBy = requestUserId;
        // to save comments
        var postComment = new modelName(postData);
        postComment.save(function (err, data) {
            if (err) return res.json(err, 400);
            res.status(200).json(data)
        })
    },

    /**
    * Action for delete comments.
    *
    * @method postComment
    * @param {req} redeleteCommentquest
    * @param {res} response
    * @return {status} res -If res is success it will delete the comment otherwise it returns false. 
    */
    deleteComment: function (req, res) {
        var modelName = models.postComment;
        var commentId = req.params.id;
        modelName.findOne({ _id: commentId }).exec(function (err, data) {
            if (typeof data !== 'undefined' && data !== '' && data !== null) {
                // delete the comment from the post
                data.remove(function (err, data) {
                    if (err) {
                        res.send(err);
                    }
                    var data = {
                        httpstatus: 200,
                        msg: appMessage.common.success.deleteComment.msg
                    }
                    res.send(data)
                })
            } else {
                var error = {
                    httpstatus: 400,
                    msg: appMessage.common.error.badRequest.msg
                };
                res.send(error)
            }
        });
    },

    /**
     * Action for updating comment
     *
     * @method editComment
     * @param {req} request
     * @param {res} response
     * @return {status} res -If it returns error then the status is false otherwise it will update the comment with success response. 
     */
    editComment: function (req, res) {
        var modelName = models.postComment;
        var condition = {
            '_id': req.params.id,
        };
        var updateParams = req.body;
        modelName.findOneAndUpdate(condition, updateParams, { new: true }).then(function (err, data) {
            if (err) return res.json(err, 400);
            res.status(200).json(data)
        });
    },
}



