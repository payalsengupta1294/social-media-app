var mobControllers = require('../mob');

/*
 * Users mobile controller route
 *
 */
app.post('/v1.0/users/registration', mobControllers.v1_0.users.addUser);
app.post('/v1.0/users/login', mobControllers.v1_0.users.login);
app.get('/v1.0/users/user-details', mobControllers.v1_0.users.getUserDetails);
app.post('/v1.0/users/add-friends', mobControllers.v1_0.users.addFriends);
app.put('/v1.0/users/update-users/:id', mobControllers.v1_0.users.editUser);

/*
 * Post mobile controller route
 *
 */
app.post('/v1.0/post/add-post', mobControllers.v1_0.post.addPost);
app.get('/v1.0/post/get-all-post', mobControllers.v1_0.post.getAllPost);
app.get('/v1.0/post/post-details/:id', mobControllers.v1_0.post.getPostDetails);
app.delete('/v1.0/post/delete-post/:id', mobControllers.v1_0.post.deletePost);
app.put('/v1.0/post/update-post/:id', mobControllers.v1_0.post.editPost);
app.post('/v1.0/post/add-like', mobControllers.v1_0.post.postLike);
app.post('/v1.0/post/add-comment', mobControllers.v1_0.post.postComment);
app.delete('/v1.0/post/delete-comment/:id', mobControllers.v1_0.post.deleteComment);
app.put('/v1.0/post/update-comment/:id', mobControllers.v1_0.post.editComment);
