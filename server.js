/*
 * dotenv setup to manage environments
 */
var argv = require('yargs')
        .command('environment', function (yargs) {
            yargs.options({
                location: {
                    demand: true,
                    alias: 'e',
                    type: 'string'
                }
            });
        })
        .help('help')
        .argv;
var envFileName = argv.e;
require('dotenv').config({path: ".env." + envFileName});


/*
 * define all global and local variables
 */
var express = require('express');
var path = require('path');
global.fs = require('fs');
global.app = express();
app.use('/public', express.static(path.join(__dirname, 'public')))
global.bodyParser = require('body-parser');
global.cors = require('cors');
global.router = express.Router();
global.appMessage = require('./app/helpers/language/' + process.env.MSGLANG + ".msg.js");
global.winston = require('winston');
global.mongoose = require('mongoose');
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
//mongo connection
mongoose.connect('mongodb://' + process.env.mongo_server, {
    useNewUrlParser: true ,
    useUnifiedTopology: true,
    useCreateIndex: true 
});
mongoose.Promise = require('bluebird');
global.Schema = mongoose.Schema;

var settings = require('./config/settings');
global._ = require('lodash');
global.models = require('./app/models/');
global.common = require('./app/common/');
require('./config/error_log_handler.js');

/**
 * For validation using middleware
 */
app.options(cors({origin: '*'}));
app.use(cors({origin: '*'}));
app.use(function (req, res, next) { 
    res.header("Access-Control-Expose-Headers", "x-access-token");
    next();
});
global.auth = require('./app/middleware/auth.js');
app.use(auth('on'));

/*
 * Add Routor
 */
require('./app/route/mobileRoutes');

var server = require('http').Server(app);
server.listen(settings.port, function () {
    console.log(("Listening on port " + settings.port));
}).on('error', function (e) {
    if (e.code == 'EADDRINUSE') {
        console.log('Address in use. Is the server already running?');
    }
});
