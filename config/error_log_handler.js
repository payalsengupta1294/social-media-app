/*access logs using morgan*/
global.morgan = require('morgan')
var accessLogStream = fs.createWriteStream('./logs/access.log', {flags: 'a'})
var errorLogStream = fs.createWriteStream('./logs/error.log', {flags: 'a'})
if(process.env.CONSOLE_ERROR_LOG == "true"){
    app.use(morgan('dev'));
}
var logger = morgan(':id :method :url :response-time')
app.use(morgan('combined', { 
    skip: function(req, res) {
        return res.statusCode == 200 
    }
    ,stream: errorLogStream,
}));
app.use(morgan('combined', { 
    skip: function(req, res) {
        return res.statusCode > 200 
    }
    ,stream: accessLogStream,
}));